# Mecum Auctions Front End Developer

This repository is to contain the 10/2015 programming problems for Front End Developer Applicants

## Problems

1.   _FizzBuzzBang_

     For numbers 1 to n where n is 120, print out each number on a single line.

     If a number is divisible by 3, 5, or 7, print "Fizz", "Buzz" or "Bang" instead of the number, 
     respectively.  If a number is divisible by two or more of these numbers (again, 3,5, and 7), you 
     should print out both/all words instead of the number.

## License and Authors

Author:: Cade Cannon (<ccannon@mecum.com>)